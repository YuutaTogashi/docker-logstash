import tailer
from useful import get_path_to_log, \
                   search_expression,\
                   process_file, \
                   collect_logfiles, \
                   pack_container, \
                   get_container_host

from flask import Flask
from flask_jsonrpc import JSONRPC
from gevent import pool
from gevent.wsgi import WSGIServer
from gevent import monkey
from flask_cors import CORS, cross_origin
monkey.patch_all()

app = Flask(__name__)
app.debug = True

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
jsonrpc = JSONRPC(app, '/api')


def grep_expression(containers=None, expression=None):
    if not containers or not expression:
        return None

    result = {}
    group = pool.Group()
    for container in containers:
        path = get_path_to_log(container['host'], container['id'])
        result[container['host']] = {}
        result[container['host']][container['id']] = {'host': get_container_host(path),
                                                      'data': []}
        group.apply_async(search_expression, (path, expression), {'result': result[container['host']]
                                                                                  [container['id']]['data']})

    group.join()
    return result


@jsonrpc.method('tail')
def tail(host, container_id, numlines=50):
    return process_file(tailer.tail, host, container_id, numlines)


@jsonrpc.method('head')
def head(host, container_id, numlines=50):
    return process_file(tailer.head, host, container_id, numlines)


@jsonrpc.method('grep')
def grep(*args, **kwargs):
    result = {}
    operations = {'expression': grep_expression}

    for operation in operations:
        if operation in kwargs:
            result[operation] = operations[operation](**kwargs[operation])
    return result


@jsonrpc.method('list')
def containers_list():
    containers = collect_logfiles()
    containers = list(map(pack_container, containers))
    return containers


if __name__ == '__main__':
    http_server = WSGIServer(('', 5000), app)
    http_server.serve_forever()
