import os
import re
import glob

from docker import Client

path_to_logs = '/tmp2/logs/docker/'
host_re = '(\d{1,3}\.){3}\d,([\w-]+)'
cli = Client(base_url='unix://var/run/docker.sock')


def get_path_to_log(host, container_id):
    return path_to_logs + '{0}/{1}.log'.format(host, container_id)


def collect_logfiles():
    return glob.iglob(path_to_logs + '**/*.log', recursive=True)


def get_container_host(container_path):
    if not container_path:
        return
    try:
        match_host = re.search(host_re, container_path).group(0)
    except AttributeError:
        match_host = None

    return str(match_host)


def get_id_from_patn(path):
    container_id = re.search(r'(((\w)+?)(?=\.log))', path)
    return container_id.group(0)


def pack_container(path):
    result = {
        'size': str(os.path.getsize(path)),
        'id': get_id_from_patn(path),
        'host': get_container_host(path)
    }
    return result


def process_file(fn, host, container_id, numlines):
    path = get_path_to_log(host, container_id)
    logfile = open(path)
    lines = fn(logfile, numlines)
    return lines


def search_expression(path, expression, result=None):
    logfile = open(path)
    for line in logfile:
        match = re.search(expression, line)
        if match:
            result.append({'line': line,
                           'match': match.group(0)})
